    close all;
    clear all;
    clc;
figure
% opening scale image
scaleImg = imread('Scale.bmp','bmp');
image(scaleImg)
% from 0 to 17  
pixelsValues = scaleImg(1,:, :,:,:);
pSize = size(pixelsValues,2);
temperatureValues = zeros(1,3);


cut_beg = 1;
cut_end = pSize;

for i = 1:pSize
    b(i) = (0 + (17-0) * (i-1))./(pSize -1);
    if b(i) > 7
        cut_end = i;
        break;
    end
end

for i = 1:pSize
    b(i) = (0 + (17-0) * (i-1))./(pSize -1);
    if b(i) > 0
        cut_beg = i;
        break;
    end
end

pixelsValues(:,cut_beg:cut_end,:,:,:) = [];

dataImage = imread('2.bmp','bmp');

% range from 0 to 7
dataImSize = size(dataImage);

h = waitbar(0,'Please wait...');
steps = length(dataImage(:,1,1,1,1));
counter = 0;


for i = 1:length(dataImage(:,1,1,1,1))
    for j = 1:length(dataImage(1,:,1,1,1))-2
        for z = 1:length(pixelsValues)
            if (pixelsValues(1,z,1) == dataImage(i,j,1) && pixelsValues(1,z,2) == dataImage(i,j,2) && pixelsValues(1,z,3) == dataImage(i,j,3))
                        dataImage(i,j,1) = 255;
                        dataImage(i,j,2) = 255;
                        dataImage(i,j,3) = 255;
                        break
            end
        end
    end
    waitbar(i/steps)
end
close(h);
image(dataImage);


