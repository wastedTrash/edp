clear all
close all
clc;

 [filename1, pathname1] = uigetfile('*.bmp', 'Choose a map');
 [filename2, pathname2] = uigetfile('*.bmp', 'Choose a scale');


% temper_begin = 5;     % �������� ���������� ���
% temper_end   = 8;     % ���������� � �����


X     = imread([pathname1 filename1]); % ������ BMP-����� � ������� �-������-������-R-G-B
Y     = imread([pathname2 filename2]);      % ������ BMP-����� � ������� Y-������-������-R-G-B
figure;
%imshow(X);
image(X);
%truesize;

prompt = {'������� min �������� �����:','������� max �������� �����:'};
dlg_title = '�������� ��������� ����� ������';
num_lines = 1;
def = {'-0.8','10.7'};

answer = inputdlg(prompt,dlg_title,num_lines,def);

Scale_min    = str2num(char(answer(1)));  %  = -0.8;  % ������� ��������
Scale_max    = str2num(char(answer(2)));  %   10.7;  % � ������
% Scale_min    = -1.1;
% Scale_max    = 7.6; 

prompt = {'������� min �������� ��� ����������:','������� max �������� ��� ����������:'};
dlg_title = '���������� ������';
num_lines = 1;
def = {'5','8'};
answer = inputdlg(prompt,dlg_title,num_lines,def);

temper_begin = str2num(char(answer(1)));     % �������� ���������� ���
temper_end   = str2num(char(answer(2)));     % ���������� � �����



tic;


%impixelregion;
Scale = Y(1,:,:,:,:);           % �������� � ������� ����� ������ Y ������ ������ ������

Count = length(Scale(1,:,1));   % ����������� ��������� Count ����� ����� ������ (= ���-�� �������� �� ������ bmp-����� ����� ������)
sc_begin = 1;                   % sc_begin - ����� ������� ������� (�������� ������ �������) � ������ ����� ������  ��� ����������� ����������
sc_end   = Count;               % sc_end   - ����� ���������� ������� (�������� ������ �������) � ������ ����� ������  ��� ����������� ����������

for i =1:Count
  b(i) = Scale_min +(Scale_max-Scale_min)*((i-1)./(Count-1));
    if b(i) > temper_end
    sc_end =i;   % sc_end - ����������� ���������� ������ ������� (�������� ������ �������) � ������ ����� ������  ��� ����������� ����������
    break;
    end;
end;

for i =1:Count
  b(i) = Scale_min +(Scale_max-Scale_min)*((i-1)./(Count-1));
    if b(i) > temper_begin
    sc_begin =i; % sc_begin - ����������� ������� ������ ������� (�������� ������ �������) � ������ ����� ������  ��� ����������� ����������
    break;
    end;
end;

  Scale(:,sc_begin:sc_end,:,:,:)=[]; % �������� ���� �������� � ������� Scale � ��������� ����� sc_begin � sc_end

h= waitbar(0,'Please wait...');
steps = length(X(:,1,1,1,1));

for i = 1:length(X(:,1,1,1,1))             % ������� �� ���� ������� ������� � (�. �. �� ���� �������� ����� ����������� - ������ �����������)
    for j = 1:length(X(1,:,1,1,1))-2       % ������� �� ���� �������� ������� � (�. �. �� ���� �������� �������� ����������� - ������ �����������)
    %    Is_present_in_scale = 0;           % Is_present_in_scale - ��������� �������� ������� ����������� ����������� ��� ������������� ��� ���������� � ����� ����
        for k = 1:length(Scale(1,:,1))
            if and(X(i,j,1) == Scale(1,k,1), and(X(i,j,2) == Scale(1,k,2), X(i,j,3) == Scale(1,k,3))) % ��������� �������� ������� ����������� �� ����� ��������� ����� �� R-G-B  
     %          Is_present_in_scale = 1;    % Is_present_in_scale - ��������� �������� ������� ����������� ����������� ��� ����������� ��� ���������� � ����� ����
                 X(i,j,1) = 255;           % ���������� �������� ������� ����������� X(i,j,1
                 X(i,j,2) = 255;           % � �����
                 X(i,j,3) = 255;           % ����     
                break;                     % ��� �������, ��� ����������� ������ (R-G-B) ������� � ������� Scale
            end;
          
        end;
%         if Is_present_in_scale ~= 0
%                  X(i,j,1) = 255;           % ���������� �������� ������� ����������� X(i,j,1
%                  X(i,j,2) = 255;           % � �����
%                  X(i,j,3) = 255;           % ����     
%         end;
    end;
    waitbar(i/steps)
end;
close(h);

tElapsed=toc;
[filename3, pathname3] = uiputfile('*.bmp', 'Choose a a file for result calculations');
imwrite(X,[pathname3 filename3]);                  % ������ ���������������� ����������� � bmp-����

figure;
%imshow(X);
image(X);

msgbox({['Calculations have taken ' num2str(tElapsed) ' seconds'];['Results in file ' [pathname3 filename3]]},'Information message','warn','non-modal');
%msgbox(['Calculations have taken ' num2str(tElapsed) ' seconds'],'Information message','warn','non-modal');


