    close all;
    clear all;
    clc;
figure
% opening scale image
scaleImg = imread('Scale.bmp','bmp');
image(scaleImg)
% from 0 to 17  
pixelsValues = scaleImg(1,:, :,:,:);
pSize = size(pixelsValues,2);
temperatureValues = zeros(1,3);
crunch = 0;
newIndex = 0;
for i = 1:pSize
    % making array of temperature values
    % you can make easier simply by working with this construction later
    % (i * 17.0)./pSize
    value = (i * 17.0)./pSize;
    if (value >= 7.0)
        newIndex = crunch + newIndex + 1;
        temperatureValues(newIndex,1) = pixelsValues(1, i,1);
        temperatureValues(newIndex,2) = pixelsValues(1, i,2);
        temperatureValues(newIndex,3) = pixelsValues(1, i,3);
    else continue;
    end
end

dataImage = imread('2.bmp','bmp');
% image(dataImage);

% range from 0 to 7
dataImSize = size(dataImage);
actualRGB = zeros(3);
steps = dataImSize(1)*dataImSize(2);
counter = 0;
% length(dataImage(:,1,1,1,1));
for i = 1:dataImSize(1)
    for j = 1:dataImSize(2)
        tic;
        for z = 1:length(temperatureValues(:,1,1))
            if temperatureValues(z,1) == dataImage(i,j,1)
                if temperatureValues(z,2) == dataImage(i,j,2)
                    if temperatureValues(z,3) == dataImage(i,j,3)
                        dataImage(i,j,1) = 255;
                        dataImage(i,j,2) = 255;
                        dataImage(i,j,3) = 255;
                        break
                    end
                end
            end
        end
        toc;
    end
    %waitbar(i/ steps);
end
image(dataImage);


